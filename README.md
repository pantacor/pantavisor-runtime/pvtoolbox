# pvtoolbox

To import it to your project, add the following snippet to your dockerfile:

*X86_64 (static binary):
```
FROM registry.gitlab.com/pantacor/pantavisor-runtime/pvtoolbox:amd64-master as pvtoolbox
```

*ARM 32-bit (static binary):
```
FROM registry.gitlab.com/pantacor/pantavisor-runtime/pvtoolbox:arm32v7-master as pvtoolbox
```

Then, you can copy the commands:

```
COPY --chown=0:0 --from=pvtoolbox /usr/local/bin/pvsocket /usr/local/bin/pvsocket
COPY --chown=0:0 --from=pvtoolbox /usr/local/bin/pvlog /usr/local/bin/pvlog
COPY --chown=0:0 --from=pvtoolbox /usr/local/bin/pvmeta /usr/local/bin/pvmeta
COPY --chown=0:0 --from=pvtoolbox /usr/local/bin/pvreboot /usr/local/bin/pvreboot
COPY --chown=0:0 --from=pvtoolbox /usr/local/bin/pvpoweroff /usr/local/bin/pvpoweroff

RUN chmod +x /usr/local/bin/pvsocket && \
	chmod +x /usr/local/bin/pvlog && \
	chmod +x /usr/local/bin/pvmeta && \
	chmod +x /usr/local/bin/pvreboot && \
	chmod +x /usr/local/bin/pvpoweroff
```
