#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <sys/un.h>
#include <fcntl.h>
#include <stdlib.h>

int open_socket(char *path)
{
	int fd, ret;
	struct sockaddr_un addr;
	long arg;

	fd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (fd < 0)
		return 1;

	memset(&addr, 0, sizeof(addr));
	addr.sun_family = AF_UNIX;

	strcpy(addr.sun_path, path);

	arg = fcntl(fd, F_GETFL, NULL);
	arg |= O_NONBLOCK;
	fcntl(fd, F_SETFL, arg);

	ret = connect(fd, (struct sockaddr*)&addr, sizeof(addr));
	while (ret < 0) {
		usleep(10000);
		ret = connect(fd, (struct sockaddr*)&addr, sizeof(addr));
	}

	if (ret)
		return 1;

	arg = fcntl(fd, F_GETFL, NULL);
	arg &= (~O_NONBLOCK);
	fcntl(fd, F_SETFL, arg);

	return fd;
}

int main(int argc, char *argv[])
{
	if ((argc != 3) || (!strncmp("--help",argv[1],6))) {
		printf("Usage: pvsocket COMMAND DATA\n\r");
		printf("Sends a command to pantavisor.\n\r");
		return 1;
	}

	int ret;
	int fd;
	char cmd[4096];
	char buf[4096];

	snprintf(cmd, 4096, "%c" "%s", argv[1][0]-'0', argv[2]);
	fd = open_socket("/pantavisor/pv-ctrl");
	write(fd, cmd, strlen(cmd));
	do {
		memset (buf, '\0', 4096);
		ret = read(fd, buf, sizeof(buf));
		if (ret < 0)
			printf("ERROR: %s\n", strerror(errno));
		else if (ret > 0 )
			printf("%s", buf);

	} while(ret >0);

	close(fd);
	return ret;
}
