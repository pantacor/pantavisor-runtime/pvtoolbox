FROM alpine:3.14 as sysroot

RUN apk update && apk --force add git build-base binutils grep findutils coreutils gcc && rm -rf /var/cache/apk/*

FROM registry.gitlab.com/pantacor/platform-tools/docker-musl-cross-x86_64 as crossbuilder

WORKDIR /work

COPY --from=sysroot / /sysroot-arm
COPY src src

RUN cd src; x86_64-linux-musl-gcc --static -o pvsocket pvsocket.c

FROM alpine:3.14

COPY --from=crossbuilder --chown=0:0 /work/src/pvsocket /usr/local/bin/pvsocket
COPY --from=crossbuilder --chown=0:0 /work/src/pvlog /usr/local/bin/pvlog
COPY --from=crossbuilder --chown=0:0 /work/src/pvmeta /usr/local/bin/pvmeta
COPY --from=crossbuilder --chown=0:0 /work/src/pvreboot /usr/local/bin/pvreboot
COPY --from=crossbuilder --chown=0:0 /work/src/pvpoweroff /usr/local/bin/pvpoweroff

RUN chmod +x /usr/local/bin/pvsocket && \
	chmod +x /usr/local/bin/pvlog && \
	chmod +x /usr/local/bin/pvmeta && \
	chmod +x /usr/local/bin/pvreboot && \
	chmod +x /usr/local/bin/pvpoweroff
